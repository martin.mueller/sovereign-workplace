<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
**Content / Quick navigation**

[[_TOC_]]

# Functional Components

Functional components are the core of the SWP as they provide it's rich functionaly. We use the community versions of the named products. For production environments please use enterprise versions for support and scalabiliy reasons.

## Groupware - Open-Xchange AppSuite

## WebOffice - Collabora Development Edition

## File & Share - Nextcloud

## Kollaboration - dOnlineZusammenarbeit 2.0

## Videokonferenzen - Jitsi

## Knowledge Management - XWiki

## Project Management - OpenProject

## IAM - Univention Corporate Services
